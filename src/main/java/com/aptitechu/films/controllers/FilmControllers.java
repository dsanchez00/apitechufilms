package com.aptitechu.films.controllers;


import com.aptitechu.films.models.FilmModel;
import com.aptitechu.films.models.ScoreModel;
import com.aptitechu.films.services.FilmService;
import com.aptitechu.films.services.FilmServiceResponse;
import com.aptitechu.films.services.RatingService;
import com.sun.xml.internal.ws.client.ResponseContextReceiver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

//El CrossOrigin es para el CORS,  esto no nos va a salir a backend sino en el navegador (frontend)
//Esto no se puede solucionar desde frontend, sino desde backend con esto.
//Origins son los servidores desde donde se permite hacer la conexion, * es todos, es solo de prueba
//methods son los metodos que se permiten
//Tambien se puede poner a nivel de métodos (en el getUsers, etc)
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
@RestController
@RequestMapping("/apitechufilms/v1")
public class FilmControllers {

    @Autowired
    FilmService filmService;

    @Autowired
    RatingService ratingService;

    //@GetMapping("/films")
    //public ResponseEntity<List<FilmModel>> getFilmsByYear(){
    //    System.out.println("getFilmsByYear en FilmController");

    //    return new ResponseEntity<>(
    //            this.filmService.findAll(),
    //            HttpStatus.OK
    //    );
    //}

    //BUSCAR TODAS LAS PELICULAS (FILTRO OPCIONAL -> MOSTRAR POR ORDEN PUNTUACION)
    @GetMapping("/films")
    public ResponseEntity<List<FilmModel>> getFilms(
            @RequestParam (name = "$orderby", required = false) String orderBy){

        System.out.println("getFilms ordenadas");

        return new ResponseEntity<>(
                this.filmService.getFilms(orderBy),
                HttpStatus.OK
        );
    }

    //BUSCAR PELICULA POR SU ID
    @GetMapping("/films/{id}")
    public ResponseEntity<Object> getFilmById(@PathVariable String id) {
        System.out.println("getFilmById");
        System.out.println("La ID de la película a buscar es: " + id);

        Optional<FilmModel> user = this.filmService.findById(id);

        return new ResponseEntity<>(
                user.isPresent() ? user.get() : "Pelicula no encontrada",
                user.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    //BUSCAMOS PELICULAS POR AÑO DE ESTRENO
    @GetMapping("/films/year/{year}")
    public ResponseEntity<Object> getFilmByYear(@PathVariable Integer year) {
        System.out.println("getFilmByYear");
        System.out.println("La ID de la película a buscar es: " + year);

        return new ResponseEntity<>(
                this.filmService.findByYear(year)
                , HttpStatus.OK
        );
    }

    @PostMapping("/films")
    public ResponseEntity<FilmModel> addFilm(@RequestBody FilmModel film){

        System.out.println("addFilm");

        return new ResponseEntity<>(
                this.filmService.add(film),
                HttpStatus.CREATED
        );
    }

    @PutMapping("/films")
    public ResponseEntity<String> modifyFilm(@RequestBody FilmModel film){
//  public ResponseEntity<FilmModel> modifyFilm(@RequestBody FilmModel film){

        System.out.println("modifyFilm");
        System.out.println("La id de la película a mod es: " + film.getIdFilm());
        System.out.println("El titulo de la película a mod es: " + film.getTitle());
        System.out.println("El año de la pelicula a mod es: " + film.getYearFilm());
        System.out.println("La imagen de la pelicula a mod es: " + film.getPhoto());
        System.out.println("La puntuacion de la pelicula a mod es: " + film.getRating());
        System.out.println("La descripcion de la pelicula a mod es: " + film.getDescription());

        FilmServiceResponse result = this.filmService.modify(film);

        return new ResponseEntity<>(
                result.getMsg(),
                result.getResponseHttpStatusCode()
//      return new ResponseEntity<>(
//              this.filmService.modify(film),
//              HttpStatus.CREATED
        );
    }

    @DeleteMapping("/films/{id}")
    public ResponseEntity<String> deleteFilm(@PathVariable String id){
        System.out.println("deleteFilm");
        System.out.println("La id de la pelicula a borrar es: " + id);

        boolean deleteUser = this.filmService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Película borrada": "Película no encontrada",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }


    @PostMapping("/ratings")
    public ResponseEntity<ScoreModel> addRating(@RequestBody ScoreModel rating){

        System.out.println("addRating");

        boolean result = true;
        FilmModel fModel = new FilmModel();
        Optional<FilmModel> filmModel = this.filmService.findById(rating.getIdFilm());

        if (filmModel.isPresent()){
            //Se actualiza la puntuacion de la peli en BBDD haciendo la media con la que viene
            System.out.println("La peli a votar existe en BBDD");
            fModel = filmModel.get();
            fModel.setRating(((fModel.getRating()+rating.getScore())/2));
            FilmServiceResponse resmod = this.filmService.modify(fModel);
        }else{
            System.out.println("La peli a votar no existe en BBDD");
            result = false;
        }

//Se inserta la votacion
        if (result){
            ScoreModel scoreModel = this.ratingService.add(rating);
        }

        return new ResponseEntity<>(
                rating,
                result ? HttpStatus.CREATED : HttpStatus.NOT_FOUND
        );
    }

    @GetMapping("/ratings/{id}")
    public ResponseEntity<List<ScoreModel>> getRatings(@PathVariable String id){
        System.out.println("getRatings en FilmController");
        System.out.println("La id de la pelicula a recuperar votaciones es: " + id);

        List<ScoreModel> ratings = this.ratingService.findById(id);

        return new ResponseEntity<>(
                ratings,
                ratings.isEmpty() ? HttpStatus.NOT_FOUND : HttpStatus.OK
        );
    }

}

package com.aptitechu.films.services;

import com.aptitechu.films.FilmsApplication;
import com.aptitechu.films.models.FilmModel;
import com.aptitechu.films.models.ScoreModel;
import com.aptitechu.films.repositories.FilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.naming.ldap.SortKey;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class FilmService {

    @Autowired
    FilmRepository filmRepository;

//  @Autowired
//  FilmService filmService;

    //BUSCAR USUARIO POR ID
    public Optional<FilmModel> findById(String id){
        System.out.println("findById en FilmService");
        return this.filmRepository.findById(id);
    }

    public List<FilmModel> getFilms(String orderBy){
        System.out.println("getUsers en UserService");
        List<FilmModel> result;

        if (orderBy != null){
            System.out.println("Se ha pedido ordenación");

            result = this.filmRepository.findAll(Sort.by("rating"));
        }else{
            System.out.println("No se ha pedido ordenación");

            result = this.filmRepository.findAll();
        }

        return result;
    }

    public List<FilmModel> findByYear(Integer yearFilm){
        System.out.println("findByYear en FilmService");

        List<FilmModel> resultado = new ArrayList<>();
        List<FilmModel> listaTemp = this.filmRepository.findAll();

        for (int j =0; j<listaTemp.size();j++){

            System.out.println("FOR numero " + j);

            System.out.println("ListaTemps" + listaTemp.get(j));

            if (listaTemp.get(j).getYearFilm().equals(yearFilm)){
                System.out.println("Dentro del IF");
                resultado.add(listaTemp.get(j));
            }
        }

        return resultado;
    }

    public FilmModel add(FilmModel film){

        System.out.println("add en FilmService");

        if (film.getPhoto().equals("")){
            film.setPhoto("https://www.qmul.ac.uk/media/qmul/media/2019/cinema640x410.jpg");
        }
        return this.filmRepository.save(film);
    }

    public FilmServiceResponse modify(FilmModel film){

        System.out.println("modify en FilmService");

        FilmServiceResponse result = new FilmServiceResponse();

        if ((film.getTitle().equals(""))||(film.getTitle() == null)||
                (film.getIdFilm().equals(""))||(film.getIdFilm() == null)||
                (!(film.getYearFilm() > 0))||(!(film.getRating() > 0.0))||
                (film.getPhoto().equals(""))||(film.getPhoto() == null)||
                (film.getDescription().equals(""))||(film.getDescription() == null)){
            System.out.println("Error, faltan campos de entrada");
            result.setMsg("Error, faltan campos de entrada");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
            return result;
//          return null;
        }else{

            Optional<FilmModel> resultFilm = this.filmRepository.findById(film.getIdFilm());

            if(resultFilm.isPresent()){
                System.out.println("Pelicula encontrada, actualizando...");
                result.setMsg("Pelicula encontrada, actualizando...");
                result.setResponseHttpStatusCode(HttpStatus.OK);
                this.filmRepository.save(film);
                return result;
            }else{
                System.out.println("Pelicula no encontrada, cancelando...");
                result.setMsg("Pelicula no encontrada, cancelando...");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);
                return result;
//              return null;
            }
        }
    }

    public boolean delete(String id) {
        System.out.println("delete en FilmService");

        boolean result = false;

        if(this.findById(id).isPresent() == true){
            System.out.println("Película encontrada, borrando...");

            this.filmRepository.deleteById(id);
            result = true;
        }

        return result;
    }
}

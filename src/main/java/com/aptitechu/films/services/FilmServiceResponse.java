package com.aptitechu.films.services;

import com.aptitechu.films.models.FilmModel;
import org.springframework.http.HttpStatus;

public class FilmServiceResponse {
    private String msg;
    private HttpStatus responseHttpStatusCode;

    public FilmServiceResponse() {
    }

    public FilmServiceResponse(String msg, FilmModel film, HttpStatus responseHttpStatusCode) {
        this.msg = msg;
        this.responseHttpStatusCode = responseHttpStatusCode;
    }

    public String getMsg() {
        return this.msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public HttpStatus getResponseHttpStatusCode() {
        return this.responseHttpStatusCode;
    }

    public void setResponseHttpStatusCode(HttpStatus responseHttpStatusCode) {
        this.responseHttpStatusCode = responseHttpStatusCode;
    }
}

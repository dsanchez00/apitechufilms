package com.aptitechu.films.services;

import com.aptitechu.films.models.FilmModel;
import com.aptitechu.films.models.ScoreModel;
import com.aptitechu.films.models.RatingModel;
import com.aptitechu.films.repositories.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RatingService {

    @Autowired
    RatingRepository ratingRepository;

    public ScoreModel add(ScoreModel scoreModel){
        System.out.println("add en FilmService");

        RatingModel ratMod = new RatingModel();
        List<ScoreModel> scoMod = new ArrayList<>();

        Optional<RatingModel> ratingModel = this.ratingRepository.findById(scoreModel.getIdFilm());

        if (ratingModel.isPresent()){
            System.out.println("La peli ya tiene votaciones");
            ratMod = ratingModel.get();
            scoMod = ratMod.getListRatings();
//          scoMod = (ratingModel.get()).getListRatings();
        }else{
            System.out.println("La peli no tiene aun votaciones");
        }

        if (scoreModel.getNameUser().equals("")){
            System.out.println("Puntuacion sin nombre, se grabara como anónima");
            scoreModel.setNameUser("Anónimo");
            scoreModel.setIdUser("A");
        }

        scoMod.add(scoreModel);
        ratMod.setIdFilm(scoreModel.getIdFilm());
        ratMod.setListRatings(scoMod);

        this.ratingRepository.save(ratMod);
        return scoreModel;
    }

    public List<ScoreModel> findById(String id){
        System.out.println("findById en RatingService");

//      RatingModel ratMod = new RatingModel();
        List<ScoreModel> scoMod = new ArrayList<>();

        Optional<RatingModel> ratingModel = this.ratingRepository.findById(id);

        if (ratingModel.isPresent()){
            System.out.println("Votaciones encontradas " + id);
//          ratMod = ratingModel.get();
//          scoMod = ratMod.getListRatings();
            scoMod = (ratingModel.get()).getListRatings();
        }else{
            System.out.println("No se encuentran votaciones para pelicula " + id);
        }
        return scoMod;
    }

}

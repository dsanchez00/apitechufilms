package com.aptitechu.films.models;

public class ScoreModel {
//idFilm es redundante pero así se puede usar esta clase como estructura de entrada desde front
    private String idFilm;
    private String idUser;
    private String nameUser;
    private float score;

    public ScoreModel() {
    }

    public ScoreModel(String idFilm, String idUser, String nameUser, float score) {
        this.idFilm = idFilm;
        this.idUser = idUser;
        this.nameUser = nameUser;
        this.score = score;
    }

    public String getIdFilm() {
        return this.idFilm;
    }

    public void setIdFilm(String idFilm) {
        this.idFilm = idFilm;
    }

    public String getIdUser() {
        return this.idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getNameUser() {
        return this.nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public float getScore() {
        return this.score;
    }

    public void setScore(float score) {
        this.score = score;
    }
}

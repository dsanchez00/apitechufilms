package com.aptitechu.films.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document (collection = "films")
public class FilmModel {

    @Id
    private String idFilm;
    private String title;
    private String photo;
    private String description;
    private Integer yearFilm;
    private float rating;

    public FilmModel() {
    }

    public FilmModel(String idFilm, String title, String description, String photo, Integer yearFilm, float rating) {
        this.idFilm = idFilm;
        this.title = title;
        this.description = description;
        this.photo = photo;
        this.yearFilm = yearFilm;
        this.rating = rating;
    }

    public String getIdFilm() {
        return this.idFilm;
    }

    public void setIdFilm(String idFilm) {
        this.idFilm = idFilm;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getYearFilm() {
        return this.yearFilm;
    }

    public void setYearFilm(Integer yearFilm) {
        this.yearFilm = yearFilm;
    }

    public float getRating() {
        return this.rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}

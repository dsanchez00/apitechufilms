package com.aptitechu.films.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "ratings")
public class RatingModel {

    @Id
    private String idFilm;
    private List<ScoreModel> listRatings;

    public RatingModel() {
    }

    public RatingModel(String idFilm, List<ScoreModel> listRatings) {
        this.idFilm = idFilm;
        this.listRatings = listRatings;
    }

    public String getIdFilm() {
        return this.idFilm;
    }

    public void setIdFilm(String idFilm) {
        this.idFilm = idFilm;
    }

    public List<ScoreModel> getListRatings() {
        return this.listRatings;
    }

    public void setListRatings(List<ScoreModel> listRatings) {
        this.listRatings = listRatings;
    }
}

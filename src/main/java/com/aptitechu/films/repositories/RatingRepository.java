package com.aptitechu.films.repositories;

import com.aptitechu.films.models.RatingModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RatingRepository extends MongoRepository<RatingModel, String> {
}

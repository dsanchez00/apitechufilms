package com.aptitechu.films.repositories;

import com.aptitechu.films.models.FilmModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepository extends MongoRepository<FilmModel, String> {

}
